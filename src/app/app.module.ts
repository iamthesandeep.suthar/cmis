import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContentComponent } from './content/content.component';
import { BaseService } from './shared/services/base.service';
import {  HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AlertService } from './shared/services/alert.service';
import { AlertComponent } from './content/directive/alert/alert.component';

import {
  MatInputModule,
  MatRadioModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatDatepickerModule,
  MatSidenavModule,
  MatTableModule,
  MatDialogModule,
  MatToolbarModule,
  MatStepperModule,
  MatTabsModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatMenuModule,
  MatSelectModule, 
  MatNativeDateModule, 
  MatNavList,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SchemeEntryComponent } from './scheme-entry/scheme-entry.component';
import { OrderEntryListComponent } from './content/orderentrylist/orderentrylist.component';
import { SchemeEntrylistComponent } from './scheme-entry/scheme-entrylist/scheme-entrylist.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    OrderEntryListComponent,
    AlertComponent,
    SchemeEntryComponent,
    SchemeEntrylistComponent,
    AdvertisementComponent,
    HeaderComponent,
    MatNavList
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule,
    
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatSidenavModule,
    MatTableModule,
    MatDialogModule,
    MatToolbarModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatMenuModule,
    MatSelectModule,
    BrowserAnimationsModule 
    
  ],
  providers: [MatDatepickerModule,BaseService,AlertService],
  bootstrap: [AppComponent]
})
export class AppModule { }
