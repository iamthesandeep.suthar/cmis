import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable, } from '@angular/core';
import { IDictionary } from '../models/dictionary';


const httpOptions = {
  headers: new HttpHeaders().set('content-type', 'application/json')
};

@Injectable()
export class BaseService {

  constructor(private readonly _httpclient: HttpClient) { }


  get(endPoint: string, params?: IDictionary<string>) {
    let httpParams;
    if (params) {
      httpParams = this.buildParams(params);
    }
    var data = this._httpclient.get(endPoint,
      { params: httpParams }).pipe(map(res =>

        JSON.parse(JSON.stringify(res))));

    return data;
  }

  post(endPoint: string, requestObject: any) {
    debugger
    return this._httpclient.post(endPoint, requestObject,{ headers: {'Accept': 'application/json'}}).pipe(map(res => JSON.stringify(res)));
    }

  /**
    * buildParams - Converts from Dictionary to HttpParams
    */
  public buildParams(params: IDictionary<string>): HttpParams {
    let httpParams = new HttpParams();

    if (params) {
      const keys: string[] = params.Keys();

      keys.forEach(key => {
        httpParams = httpParams.append(key, params.Item(key));
      });
    }

    return httpParams;
  }



}
