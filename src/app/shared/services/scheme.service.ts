import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { AppSetting } from '../models/appsetting';
import { SchemeModel } from '../models/scheme-model';

@Injectable({
  providedIn: 'root'
})
export class SchemeService {

  constructor(private readonly _baseService: BaseService, ) { }

  PostData(model: SchemeModel): any {

    var frdata = new FormData();
    if (model.FileCollection != null && model.FileCollection.length > 0) {
      model.FileCollection.forEach(item => {
        frdata.append(item.Type, item.files)

      });
    }
    frdata.append("data", JSON.stringify(model))
    var result = this._baseService.post(AppSetting.BaseApiUrl + AppSetting.PostSchemeDataUrl, frdata);
    return result;
  }

  GetSchemeList() {
    var result = this._baseService.get(AppSetting.BaseApiUrl + AppSetting.SchemeListUrl, null);
    return result;
  }

}
