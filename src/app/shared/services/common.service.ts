import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { AppSetting } from '../models/appsetting';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private readonly _baseService: BaseService,) { }

  GetAllDDL(model: any) {
    var result = this._baseService.get(AppSetting.BaseApiUrl + AppSetting.GetDDlUrl+model);
    return result;
  }

  ScrollingTop(){
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
          window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
          window.clearInterval(scrollToTop);
      }
  }, 16);
  }

}
