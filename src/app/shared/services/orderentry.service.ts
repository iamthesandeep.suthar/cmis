import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { AppSetting } from '../models/appsetting';


@Injectable({
  providedIn: 'root'
})
export class OrderEntryService {

  constructor(private readonly _baseService: BaseService,) { }

  GetOrderEntryList() {
    var result = this._baseService.get(AppSetting.BaseApiUrl + AppSetting.RelatedToOrderUrl,null);
    return result;
  }

  AddOrderEntry(model: any,file:File) {
    debugger
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('Data',JSON.stringify(model));
    formData.append("enctype","multipart/form-data");

    var result = this._baseService.post(AppSetting.BaseApiUrl + AppSetting.OrderEntryUrl,formData
    );
    return result;
  }

}
