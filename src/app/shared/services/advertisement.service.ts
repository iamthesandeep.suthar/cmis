import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { AppSetting } from '../models/appsetting';
import { Advertisement } from '../models/advertisement.model';


@Injectable({
  providedIn: 'root'
})
export class AdvertisementService {

  constructor(private readonly _baseService: BaseService,) { }

  AddAdvertisement(model: Advertisement) {
    debugger
if(model.AdminDepartment)
model.AdminDepartment=model.AdminDepartment.toString();

if(model.BeneficiaryCategories)
model.BeneficiaryCategories=model.BeneficiaryCategories.toString();

if(model.Districts)
model.Districts=model.Districts.toString();

    const formData: FormData = new FormData();
    formData.append('file', model.File);
    formData.append('Data',JSON.stringify(model));
    formData.append("enctype","multipart/form-data");

    var result = this._baseService.post(AppSetting.BaseApiUrl + AppSetting.AdvertisementAddUrl,formData
    );
    return result;
  }

}
