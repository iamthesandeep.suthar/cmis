export class SchemeModel {
Id:string;
SchemeNameHin:string;
SchemeNameEng:string;
Logo:string;
Description:string;
FileAttachment :string;
MadeOfApplication:string;
MadeOfApplicationtxt:string;
CategoryOfScheme :string;
BeneficiaryType :string;
IsRelativeWithOrder:string;
NoOfEffectivePeople:number;
SchemeForms :string;
ListOfRequiredDocs:string;
ChargabaleScheme : string;
ModeOfPayment :  string; 
PaymentType :  string;
PaymentThrough :  string;
TypeOfScheme :string;
SchemeTitle :string;
ExecutiveDepartment :string;
ConcernedDepartment :string;
HelplineNo : number;
Nodal :string;
Where : string;
StateLevel : string;
DeliveryTime :Date;
PaymentOperation:string;
SchemeExpritation : string;
EndLevelDepartment :string;
FileCollection :filecollection[]=[]; //for all file
OnlineApplicationType: string;
IsserviceFee:string;
ServiceAmount:string;
NodalOfficerDetail: string;
SchemeRenewal: string;
Age:string;
Income:string;
Fees:string;
Gender:string;
SchemeValidity:string;
SchemeOutput:string;
}
export class filecollection
{
    Type:string;
    files:File=null;
}


export class SchmeListModel
{
    SchemeNameHin:string;
    SchemeNameEng:string;
    Description:string;
    IsRelativeWithOrder:string;
    SNo:number;
}

