export class Advertisement
{
    Category?:number;
    SubCategory?:number;
    SubjectEng:string;
    SubjectHin:string;
    AdvDate:Date;
    AdminDepartment:string;
    Districts:string;
    BeneficiaryCategories:string;
    File:File;
}