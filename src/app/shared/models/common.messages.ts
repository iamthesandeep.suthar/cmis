export class GlobalMessagesModel{
    static  saveSuccess: string = "Record added successfully";
    static  saveFaild: string = "Record not Saved";
    static  saveError: string = "Getting error, please try again....!";
    static  updateSuccess: string="Update record successfully";
    static  updateError: string = "Getting error, please try again....!";
    static  deleteSuccess: string = "Record deleted successfully";
    static  deleteError: string = "Getting error, please try again....!";
}