export class DDLModel {
    ddlEntry: DdlItemModel[];
    ddlDepartment:DdlItemModel[];
    ddlBeneficiarytype:DdlItemModel[];
    ddlExecutiveDepartment:DdlItemModel[];
    ddlConcernedDepartment:DdlItemModel[];
    ddlNodel:DdlItemModel[];
    ddlTypeScheme:DdlItemModel[];
    ddlCategoryScheme:DdlItemModel[];
    ddlDistrict:DdlItemModel[];
    ddlSector:DdlItemModel[];
  }
  
  export class DdlItemModel {
    Value: any;
    Name: string;
  }
  export class ColumnHeaderModel
{
    Value:string;
    Text:string;
  
}
