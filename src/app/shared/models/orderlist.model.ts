export class OrderEntryModel {
    Id : number;
	OrderType : number;
	Title : string;
	Description : string;
	Department:number;
	DepartmentAffected :number;
	ReferencesLink : string;
	TitleIsPublicOrCUG:string;
	TitleIsGOROrGOI :string;
	OrderNo :number;
	RelatedOrderNo :number;
	RelatedScheme :number;
	Category :number;
	SearchText:string;
	Sector :number;
	DocumentUrl :string;
	DateOfEntry : Date;
	DateofIssue : Date;
	OrderDate : Date;
	RelatedToOrderList:OrderRelatedToModel[]=[];
  }
  
  export class OrderRelatedToModel {
    Id :number;
    OrderEntryID :number;
	RelatedTo :number;
    paragraph  :string;
	Description :string;
	OrderRelatedToModel()
	{
	   this.Id=0;
	   this.RelatedTo=0;
	   this.paragraph="";
	   this.Description="";
	}
  }

  export class OrderEntryListModel{
	SNo:number;
	SearchText:string;
	Description:string;
	Title:string;
	DepartmentName:string;
  }


