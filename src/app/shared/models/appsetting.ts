export class AppSetting {
    static BaseApiUrl: string ="http://localhost:50237/api/";//"http://windowsdemo.projectstatus.co.uk/statedashboardapi/api/"; //"http://localhost:50237/api/";  // "http://192.168.7.25:5000/api/";// 
     

    static RelatedToOrderUrl: string ="OrderEntry/GetOrderEntryList";
   
    static OrderEntryUrl: string ="OrderEntry/CreateOrderEntry";

    static GetDDlUrl: string ="CommonDropDown/AllDropDown?keys=";

    static DDLKeyForOrderEntry: string ="ddlEntry,ddlDepartment,ddlSector,ddlBeneficiarytype";

    static DDLKeyForDistrictEntry: string ="ddldistrict";

    static DDLKeyForScheme: string ="ddlBeneficiarytype,ddlExecutiveDepartment,ddlConcernedDepartment,ddlNodel,ddlTypeScheme,ddlCategoryScheme";

    static DDLKeyForAdvertisement: string ="ddlCategoryScheme,ddlBeneficiarytype,ddlExecutiveDepartment,ddldistrict";


    static PostSchemeDataUrl: string ="Scheme/CreateScheme";

    static SchemeListUrl: string ="Scheme/GetSchemeList";

    static AdvertisementAddUrl: string ="AdvertisementTest/CreateAdvertisement";
}