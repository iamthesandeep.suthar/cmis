import { Component, OnInit, ViewChild } from '@angular/core';
import { SchemeService } from 'src/app/shared/services/scheme.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { SchmeListModel } from 'src/app/shared/models/scheme-model';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ColumnHeaderModel } from 'src/app/shared/models/commonddl.model';

@Component({
  selector: 'app-scheme-entrylist',
  templateUrl: './scheme-entrylist.component.html',
  styleUrls: ['./scheme-entrylist.component.css'],
  providers: [SchemeService]
})
export class SchemeEntrylistComponent implements OnInit {
  schemeList:SchmeListModel[];
  dataSource :any;

  displayedColumns: string[] = ['SNo','Description','IsRelativeWithOrder','SchemeNameEng','SchemeNameHin'];
  ViewdisplayedColumns: ColumnHeaderModel[] = [{Value:'SNo',Text:'No.'},{Value:'Description',Text:'Description'},{Value:'IsRelativeWithOrder',Text:'Is Relative With Order'},{Value:'SchemeNameEng',Text:'Scheme Name (English)'},{Value:'SchemeNameHin',Text:'Scheme Name (Hindi)'}];

  columnsToDisplay: string[] = this.displayedColumns.slice();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private readonly _schemeService: SchemeService,
    private readonly _alertService: AlertService) { 

  }

  ngOnInit() {
    this.GetList();
  }

  GetList(){
    debugger
    this._schemeService.GetSchemeList().subscribe(
      data => {
        debugger
        if (
          (data)
        )
      this.schemeList = <SchmeListModel[]>data;
      this.dataSource = new MatTableDataSource<SchmeListModel>(this.schemeList);
      this.dataSource.paginator = this.paginator;

      },
      error => {
        debugger
        this._alertService.error(error.message);      
      }
    );
  }

}
