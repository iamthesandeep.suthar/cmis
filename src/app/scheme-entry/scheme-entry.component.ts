import { Component, OnInit, OnDestroy } from '@angular/core';
import { SchemeModel, filecollection } from '../shared/models/scheme-model';
import { SchemeService } from '../shared/services/scheme.service';
import { CommonService } from '../shared/services/common.service';
import { AppSetting } from '../shared/models/appsetting';
import { DDLModel } from '../shared/models/commonddl.model';
import { AlertService } from '../shared/services/alert.service';
import { GlobalMessagesModel } from '../shared/models/common.messages';
import { Subscription } from 'rxjs/internal/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scheme-entry',
  templateUrl: './scheme-entry.component.html',
  styleUrls: ['./scheme-entry.component.css'],
  providers: [SchemeService, CommonService]
})
export class SchemeEntryComponent implements OnInit {
  model: SchemeModel;
  Fcollection: filecollection;
  sub: Subscription;
  CategorySchemeList: [];
  BeneficiaryList: [];
  SchemeTypeList: [];
  todayDate = new Date();
  ExecutiveDepartmentList: [];
  ConcernedDepartmentList: [];
  NodelList: [];
  dDLList: DDLModel;
  constructor(private readonly _schemeService: SchemeService,
    private readonly _commonService: CommonService,
    private readonly _alertService: AlertService,
    private _router: Router
  ) {
    this.todayDate.setDate(this.todayDate.getDate());
    this.model = new SchemeModel();
  }

  ngOnInit() {
    this.GetDDLList();
  }

  // ngOnDestroy()
  // {
  //   debugger;
  //   this.sub.unsubscribe();
  //   this.model=new SchemeModel();
  //   //this._schemeService.PostData
  // }


  Numberonly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


  saveData() {
    
    this.sub = this._schemeService.PostData(this.model).subscribe(
      (result: boolean) => {
        if (result) {

          this.ngOnInit();
          this._router.navigate(['/schemelist']);
          this._alertService.success(GlobalMessagesModel.saveSuccess);
        }
        else {
          this._commonService.ScrollingTop();
          this._alertService.error(GlobalMessagesModel.saveFaild);
        }
      },
      (error: any) => {
        this._alertService.error(error.message);
      }
    );
  }
  GetDDLList() {
    this._commonService.GetAllDDL(AppSetting.DDLKeyForScheme).subscribe(
      data => {
        if (
          (data)
        )
          this.dDLList = <DDLModel>data;
      },
      error => {
        this._alertService.error(error.message);
      }
    );
  }

  public handleFileInput(files: FileList, fileType: string) {
    // debugger;
    let isExist = false;
    this.model.FileCollection.forEach(element => {
      if (element.Type == fileType) {
        isExist = true;
        element.files = files.item(0);
      }
    });
    if (!isExist) {
      this.Fcollection = new filecollection();
      this.Fcollection.Type = fileType;
      this.Fcollection.files = files.item(0);
      this.model.FileCollection.push(this.Fcollection);
    }
  }
}

