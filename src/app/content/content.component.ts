import { Component, OnInit } from '@angular/core';
import { OrderEntryService } from '../shared/services/orderentry.service';
import { OrderEntryModel, OrderRelatedToModel } from '../shared/models/orderlist.model';
import { AlertService } from '../shared/services/alert.service';
import { GlobalMessagesModel } from '../shared/models/common.messages';
import { Validators, FormControl } from '@angular/forms';
import { DDLModel, DdlItemModel } from '../shared/models/commonddl.model';
import { AppSetting } from '../shared/models/appsetting';
import { CommonService } from '../shared/services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
  providers: [OrderEntryService,CommonService]
})
export class ContentComponent implements OnInit {
  fileToUpload: File = null;
  dDLList:DDLModel;
  relatedToOrderList: DdlItemModel[] = [];
  submitted = false;

  // Date validation 
  tomorrow = new Date(); 
  serializedDate = new FormControl((new Date()).toISOString());
  //Reference URL validation 
  public reg = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/])?(\s))?/;
  url = new FormControl('', [Validators.required, Validators.pattern(this.reg)]);
  SearchText = new FormControl('', [Validators.required]);
  title = new FormControl('', [Validators.required]);
  description = new FormControl('', [Validators.required]);
  fileValidationMsg: string;

  model: OrderEntryModel;
  OrderRelated:OrderRelatedToModel;
  
  constructor(  private readonly _orderEntryService: OrderEntryService,
    private readonly _alertService: AlertService,
    private readonly _commonService:CommonService,
    private _router: Router,
    ) { 
    this.model=new OrderEntryModel();
    this.tomorrow.setDate((this.tomorrow.getDate() + 1)-1);      
    this.OrderRelated=new OrderRelatedToModel();
    this.model.RelatedToOrderList.push(this.OrderRelated);
    this.model.TitleIsGOROrGOI="GOI";
  }

  ngOnInit() {
    this.GetDDLList();
   // this.model.OrderDate =;
  }

  handleFileInput(files: FileList) {
    if(files.item(0).type.match('image/jp.*')||files.item(0).type.match('application/pdf')|| files.item(0).type.match('application/text')) {
      this.fileValidationMsg="";
      this.fileToUpload = files.item(0);
  }
  else{
    this.fileValidationMsg="only *pdf,*jpg*,*text";
  }
    
  }

GetDDLList(){
  this._commonService.GetAllDDL(AppSetting.DDLKeyForOrderEntry).subscribe(
    data => {
      if (
        (data)
      )
      this.dDLList = <DDLModel>data;
    },
    error => {
      this._alertService.error(error.message);      
    }
  );
}

Saveclick()
{
   this.SearchText.markAsTouched();
   this.title.markAsTouched();
   this.description.markAsTouched();

  // // stop here if form is invalid
   if (this.SearchText.valid && this.title.valid && this.description.valid) {

this._orderEntryService.AddOrderEntry(this.model,this.fileToUpload).subscribe(data => {
 
  if(data){
     this.model=new OrderEntryModel();
    this.OrderRelated=new OrderRelatedToModel(); 
    this.model.RelatedToOrderList.push(this.OrderRelated);
    this.SearchText.markAsUntouched();
    this.title.markAsUntouched();
    this.description.markAsUntouched();
    this.url.markAsUntouched();
    this._alertService.success(GlobalMessagesModel.saveSuccess); 
    this._router.navigate(['/orderlist']);
  }
  else {
   
    this._commonService.ScrollingTop();
    this._alertService.error(GlobalMessagesModel.saveFaild); 
  }
  }, error => {
   
    console.log(error);
    this._alertService.error(error.message);   
  });
 }
}

AddMoreItems()
{
  this.OrderRelated=new OrderRelatedToModel();
  this.OrderRelated.RelatedTo=0;
  this.OrderRelated.paragraph="";
  this.OrderRelated.Description="";
  this.model.RelatedToOrderList.push(this.OrderRelated);  
}

RemoveClick(index){
  this.model.RelatedToOrderList.splice(index,1);
}


}
