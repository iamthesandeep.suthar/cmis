import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderEntryService } from 'src/app/shared/services/orderentry.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { OrderEntryListModel } from 'src/app/shared/models/orderlist.model';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ColumnHeaderModel } from 'src/app/shared/models/commonddl.model';


@Component({
  selector: 'app-orderentrylist',
  templateUrl: './orderentrylist.component.html',
  styleUrls: ['./orderentrylist.component.css'],
  providers: [OrderEntryService]
})
export class OrderEntryListComponent implements OnInit {
 
  orderEntryList:OrderEntryListModel[];

  dataSource :any;

  displayedColumns: string[] = ['SNo','Title','SearchText','DepartmentName','Description'];
  ViewdisplayedColumns: ColumnHeaderModel[] = [{Value:'SNo',Text:'No.'},{Value:'Title',Text:'Title'},{Value:'SearchText',Text:'Search Text'},{Value:'DepartmentName',Text:'Department Name'},{Value:'Description',Text:'Description'}];

  columnsToDisplay: string[] = this.displayedColumns.slice();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor( private readonly _orderEntryService: OrderEntryService,
    private readonly _alertService: AlertService) { 
  
  }

  ngOnInit() {
  this.GetList();
  }

  GetList(){
   
    this._orderEntryService.GetOrderEntryList().subscribe(
      data => {
       
        if (
          (data)
        )
        this.orderEntryList = <OrderEntryListModel[]>data;
      this.dataSource = new MatTableDataSource<OrderEntryListModel>(this.orderEntryList);
      this.dataSource.paginator = this.paginator;

      },
      error => {
       
        this._alertService.error(error.message);      
      }
    );
  }

}
