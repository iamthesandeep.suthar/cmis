import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { AppComponent } from './app.component';
import { SchemeEntryComponent } from './scheme-entry/scheme-entry.component';
import { OrderEntryListComponent } from './content/orderentrylist/orderentrylist.component';
import { SchemeEntrylistComponent } from './scheme-entry/scheme-entrylist/scheme-entrylist.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';


const routes: Routes = [
  {path: '' , component: ContentComponent},
   { path: 'order' , component: ContentComponent},
   { path: 'orderlist' , component: OrderEntryListComponent},
  {path: 'scheme' , component: SchemeEntryComponent},
  {path: 'schemelist' , component: SchemeEntrylistComponent},
  {path: 'advertisement' , component: AdvertisementComponent}
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
