import { Component, OnInit } from '@angular/core';
import { Advertisement } from '../shared/models/advertisement.model';
import { AlertService } from '../shared/services/alert.service';
import { Router } from '@angular/router';
import { AdvertisementService } from '../shared/services/advertisement.service';
import { GlobalMessagesModel } from '../shared/models/common.messages';
import { AppSetting } from '../shared/models/appsetting';
import { CommonService } from '../shared/services/common.service';
import { DDLModel } from '../shared/models/commonddl.model';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.css']
})
export class AdvertisementComponent implements OnInit {
  model: Advertisement;
  fileValidationMsg: string;
  dDLList: DDLModel;
  constructor(
    private readonly _alertService: AlertService,
    private _router: Router,
    private readonly _advertismentService: AdvertisementService,
    private readonly _commonService: CommonService
  ) {
    this.model = new Advertisement();
  }

  ngOnInit() {
    this.GetDDLList();
  }

  handleFileInput(files: FileList) {
    if (files.item(0).type.match('image/jp.*') || files.item(0).type.match('application/pdf') || files.item(0).type.match('application/text')) {
      this.fileValidationMsg = "";
      this.model.File = files.item(0);
    }
    else {
      this.fileValidationMsg = "only *pdf,*jpg*,*text";
    }

  }

  SaveClick() {
    debugger
    var temp = this.model;
    this._advertismentService.AddAdvertisement(this.model).subscribe(data => {

      if (data) {
        debugger;
        this._alertService.success(GlobalMessagesModel.saveSuccess);

        this.model = new Advertisement();
      }
      else {
        this._alertService.error(GlobalMessagesModel.saveFaild);
      }
    }, error => {
      debugger
      console.log(error);
      this._alertService.error(error.message);
    });


  }

  GetDDLList() {
    this._commonService.GetAllDDL(AppSetting.DDLKeyForAdvertisement).subscribe(
      data => {
        if (
          (data)
        )
          this.dDLList = <DDLModel>data;
      },
      error => {
        this._alertService.error(error.message);
      }
    );
  }


}
